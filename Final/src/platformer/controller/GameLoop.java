package platformer.controller;

import platformer.Main;
import platformer.model.*;
import platformer.model.Character;
import platformer.view.Platform;

import javax.swing.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class GameLoop extends JFrame implements Runnable {

    private Platform platform;
    private int frameRate;
    private float interval;
    private boolean running;
    private Clock clock = null ;

    public GameLoop(Platform platform) {
        this.platform = platform;
        frameRate = 10;
        interval = 1000.0f / frameRate; // 1000 ms = 1 second
        running = true;
        this.clock = new Clock() ;
    }

    private void update(Character character, Character2 character2) {


        if (platform.getKeys().isPressed(character.getLeftKey()) || platform.getKeys().isPressed(character.getRightKey())) {
            character.getImageView().tick();
            character.getEnemy();
        }

        if (platform.getKeys().isPressed(character.getLeftKey())) {
            character.setScaleX(-1);
            character.moveLeft();
            character.trace();
        }

        if (platform.getKeys().isPressed(character.getRightKey())) {
            character.setScaleX(1);
            character.moveRight();
            character.trace();
        }

        if (!platform.getKeys().isPressed(character.getLeftKey()) && !platform.getKeys().isPressed(character.getRightKey())) {
            character.stop();
        }

        if (platform.getKeys().isPressed(character.getUpKey())) {
            character.jump();
        }


        if (platform.getKeys().isPressed(character2.getLeftKey()) || platform.getKeys().isPressed(character2.getRightKey())) {
            character2.getImageView().tick();
            character2.getEnemy();
        }

        if (platform.getKeys().isPressed(character2.getLeftKey())) {
            character2.setScaleX(-1);
            character2.moveLeft();
            character2.trace();
        }

        if (platform.getKeys().isPressed(character2.getRightKey())) {
            character2.setScaleX(1);
            character2.moveRight();
            character2.trace();
        }

        if (!platform.getKeys().isPressed(character2.getLeftKey()) && !platform.getKeys().isPressed(character2.getRightKey())) {
            character2.stop();
        }

        if (platform.getKeys().isPressed(character2.getUpKey())) {
            character2.jump();
        }
    }


    private void updateScore(Character mainCharacter, Score score, Character2 mainCharacter2, Score2 score2) {

        javafx.application.Platform.runLater(() -> {
            score.setPoint(mainCharacter.getScore());
        });
        javafx.application.Platform.runLater(() -> {
            score2.setPoint2(mainCharacter2.getScore2());
        });
    }


    @Override
    public void run() {
        while (running) {

            float time = System.currentTimeMillis();

            update(platform.getMainCharacter(), platform.getMainCharacter2());
            updateScore(platform.getMainCharacter(), platform.getScore(), platform.getMainCharacter2(), platform.getScore2());


            time = System.currentTimeMillis() - time;

            if (clock.CheckIsClear() ) {
                System.out.println("GAME OVER") ;
                final JDialog frame = new JDialog() ;
                frame.setAlwaysOnTop(true) ;

                JOptionPane.showMessageDialog(frame, "Time's up!Game over") ;
                System.exit(0) ;
            }

            if (time < interval) {
                try {
                    Thread.sleep((long) (interval - time));
                } catch (InterruptedException ignore) {
                }

            } else {
                try {
                    Thread.sleep((long) (interval - (interval % time)));
                } catch (InterruptedException ignore) {
                }
            }
        }
    }

}
